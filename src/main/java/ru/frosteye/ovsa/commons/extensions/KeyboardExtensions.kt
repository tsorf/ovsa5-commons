package ru.frosteye.ovsa.commons.extensions

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment

fun View.hideSoftKeyboard() {
    val inputService = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputService.hideSoftInputFromWindow(this.windowToken, 0)
    clearFocus()
}

fun Activity.hideSoftKeyboard() {
    currentFocus?.hideSoftKeyboard()
}

fun Fragment.hideSoftKeyboard() {
    activity?.currentFocus?.hideSoftKeyboard()
}