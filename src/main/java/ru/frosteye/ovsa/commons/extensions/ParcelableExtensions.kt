package ru.frosteye.ovsa.commons.extensions

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable


fun Intent.putExtra(parcelable: Parcelable) {
    this.putExtra(parcelable::class.java.name, parcelable)
}

inline fun <reified T : Parcelable> Intent.getParcelable(): T? {
    return getParcelableExtra(T::class.java.name)
}

fun Bundle.put(parcelable: Parcelable) {
    this.putParcelable(parcelable::class.java.name, parcelable)
}

inline fun <reified T : Parcelable> Bundle.getParcelable(): T? {
    return this.getParcelable(T::class.java.name)
}

inline fun <reified T : Parcelable> Activity.getIntentParcelable(): T? {
    return intent.getParcelableExtra(T::class.java.name)
}