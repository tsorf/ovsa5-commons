package ru.frosteye.ovsa.commons.extensions

import android.widget.TextView

fun TextView.isBlank(): Boolean {
    return text.toString().isBlank()
}

fun TextView.isNotBlank(): Boolean {
    return text.toString().isNotBlank()
}

fun TextView.trimmedOrNull(): String? {
    val text = text.toString().trim()
    return if (text.isBlank()) null else text
}