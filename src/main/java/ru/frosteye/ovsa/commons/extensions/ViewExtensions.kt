package ru.frosteye.ovsa.commons.extensions

import android.app.Activity
import android.content.res.Resources
import android.util.TypedValue
import android.view.View
import androidx.activity.addCallback
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

fun View.onBackPressed() {
    (context as? Activity)?.onBackPressed()
}

fun View.clearDecorViewEffects(systemUiFlags: Int = View.SYSTEM_UI_FLAG_VISIBLE) {
    val activity = (context as? AppCompatActivity) ?: return
    activity.clearDecorViewEffects(systemUiFlags)
}

fun View.setStatusBarColor(@ColorInt color: Int) {
    val activity = (context as? AppCompatActivity) ?: return
    activity.window.statusBarColor = color
}

fun View.setSystemUiFlags(flags: Int, append: Boolean = false) {
    val activity = (context as? AppCompatActivity) ?: return
    activity.setSystemUiFlags(flags, append)
}

fun Fragment.setOnBackPressedListener(action: () -> Boolean) {
    requireActivity()
        .onBackPressedDispatcher
        .addCallback {
            val result = action()
            if (!result) {
                requireActivity().onBackPressed()
            }
        }
}