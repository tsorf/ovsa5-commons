package ru.frosteye.ovsa.commons.extensions

import android.text.Editable
import android.widget.EditText
import androidx.core.widget.doAfterTextChanged

fun registerTextChangeListeners(action: (Editable) -> Unit, vararg params: EditText) {
    params.forEach {
        it.doAfterTextChanged { e ->
            e?.let(action)
        }
    }
}