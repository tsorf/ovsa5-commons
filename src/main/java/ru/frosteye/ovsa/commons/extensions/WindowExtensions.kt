package ru.frosteye.ovsa.commons.extensions

import android.graphics.drawable.ColorDrawable
import android.view.Window
import androidx.annotation.ColorInt

fun Window.setBackgroundColor(@ColorInt color: Int) {
    setBackgroundDrawable(ColorDrawable(color))
}