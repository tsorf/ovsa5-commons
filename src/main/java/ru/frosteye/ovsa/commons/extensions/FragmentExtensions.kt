package ru.frosteye.ovsa.commons.extensions

import android.app.Activity
import android.content.Intent
import android.os.Parcelable
import android.view.View
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment


fun Fragment.startActivity(clazz: Class<out Activity>) {
    val intents = Intent(requireContext(), clazz)
    startActivity(intents)
}


fun Fragment.startActivityAndClearTask(clazz: Class<out Activity>) {
    val intents = Intent(requireContext(), clazz)
    startActivityAndClearTask(intents)
}

fun Fragment.startActivityAndClearTask(intents: Intent) {
    intents.addFlags(
        Intent.FLAG_ACTIVITY_NEW_TASK
                or Intent.FLAG_ACTIVITY_CLEAR_TOP
                or Intent.FLAG_ACTIVITY_CLEAR_TASK
    )
    startActivity(intents)
    activity?.finish()
}

fun Fragment.onBackPressed() {
    activity?.onBackPressed()
}

fun Fragment.requireAppCompatActivity(): AppCompatActivity {
    return requireActivity() as AppCompatActivity
}

fun Fragment.clearDecorViewEffects(systemUiFlags: Int = View.SYSTEM_UI_FLAG_VISIBLE) {
    val activity = (context as? AppCompatActivity) ?: return
    activity.clearDecorViewEffects(systemUiFlags)
}

fun Fragment.setStatusBarColor(@ColorInt color: Int) {
    val activity = (context as? AppCompatActivity) ?: return
    activity.window.statusBarColor = color
}

fun Fragment.setSystemUiFlags(flags: Int, append: Boolean = false) {
    val activity = (context as? AppCompatActivity) ?: return
    activity.setSystemUiFlags(flags, append)
}

inline fun <reified T : Parcelable> Fragment.requiredArgument(): Lazy<T> {
    return lazy {
        val argument: T? = arguments?.getParcelable()
        requireNotNull(argument) {
            "No required argument with type ${T::class.java} provided"
        }
        argument
    }
}