package ru.frosteye.ovsa.commons.extensions

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.os.Parcelable
import android.util.TypedValue
import android.view.View
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment


fun Activity.startActivity(clazz: Class<out Activity>) {
    val intents = Intent(this, clazz)
    startActivity(intents)
}

fun Activity.startActivityAndClearTask(clazz: Class<out Activity>) {
    val intents = Intent(this, clazz)
    startActivityAndClearTask(intents)
}

fun Activity.startActivityAndClearTask(intents: Intent) {
    intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
            or Intent.FLAG_ACTIVITY_CLEAR_TOP
            or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    startActivity(intents)
    finish()
}

fun AppCompatActivity.enableBackButton(toolbar: Toolbar) {
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    toolbar.setNavigationOnClickListener {
        onBackPressed()
    }
}

fun AppCompatActivity.clearDecorViewEffects(systemUiFlags: Int = View.SYSTEM_UI_FLAG_VISIBLE) {
    val theme: Resources.Theme = theme
    val statusBarColorValue = TypedValue()
    theme.resolveAttribute(android.R.attr.statusBarColor, statusBarColorValue, true)
    @ColorInt val color: Int = statusBarColorValue.data
    window.statusBarColor = color
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
        val lightStatusBarValue = TypedValue()
        theme.resolveAttribute(
            android.R.attr.windowLightStatusBar,
            lightStatusBarValue,
            true
        )
        val lightStatusBar = lightStatusBarValue.data
        if (lightStatusBar != 0) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or systemUiFlags
        } else {
            window.decorView.systemUiVisibility = systemUiFlags
        }
    }
}

fun AppCompatActivity.setSystemUiFlags(flags: Int, append: Boolean = false) {
    if (append) {
        window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or flags
    } else {
        window.decorView.systemUiVisibility = flags
    }
}



inline fun <reified T : Parcelable> Activity.requiredArgument(): Lazy<T> {
    return lazy {
        val argument: T? = intent?.getParcelable()
        requireNotNull(argument) {
            "No required argument with type ${T::class.java} provided"
        }
        argument
    }
}