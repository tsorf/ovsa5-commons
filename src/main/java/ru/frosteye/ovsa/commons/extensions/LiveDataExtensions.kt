package ru.frosteye.ovsa.commons.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

fun <T> LifecycleOwner.observe(source: LiveData<T>, observer: (T) -> Unit) {
    source.observe(this, observer)
}