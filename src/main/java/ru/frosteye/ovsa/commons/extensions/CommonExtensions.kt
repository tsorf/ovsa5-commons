package ru.frosteye.ovsa.commons.extensions

inline fun <reified T>Any?.safeCast(block: T.() -> Unit = {}): T? {
    if (this == null) {
        return null
    }
    return if (this is T) {
        block(this)
        this
    } else {
        null
    }
}
inline fun <reified T>Any?.safeCast(): T? {
    if (this == null) {
        return null
    }
    return if (this is T) {
        this
    } else {
        null
    }
}