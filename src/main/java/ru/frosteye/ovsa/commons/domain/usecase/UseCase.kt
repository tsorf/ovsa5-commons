package ru.frosteye.ovsa.commons.domain.usecase

import io.reactivex.rxjava3.core.Observable


interface UseCase<Param, Result : Any> {

    fun build(param: Param): Observable<Result>
}