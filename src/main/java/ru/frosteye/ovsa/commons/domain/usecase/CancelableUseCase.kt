package ru.frosteye.ovsa.commons.domain.usecase

interface CancelableUseCase<Param, Result : Any> : UseCase<Param, Result> {

    fun cancel()
}