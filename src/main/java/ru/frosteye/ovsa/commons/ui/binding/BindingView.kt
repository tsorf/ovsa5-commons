package ru.frosteye.ovsa.commons.ui.binding

import android.view.LayoutInflater
import android.view.View
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

interface BindingView<B : ViewBinding> {

    val boundView: B
}

inline fun <reified B> BindingView<B>.binding(
    noinline viewProvider: () -> View
): ViewBindingLazy<B> where B : ViewBinding {
    return ViewBindingLazy(B::class.java, viewProvider)
}

inline fun <reified B> BindingView<B>.activityBinding(
    noinline layoutInflaterProvider: () -> LayoutInflater
): LayoutInflaterBindingLazy<B> where B : ViewBinding {
    return LayoutInflaterBindingLazy(B::class.java, layoutInflaterProvider)
}

class ViewBindingLazy<B : ViewBinding>(
    private val clazz: Class<B>,
    private val viewProvider: () -> View
) : ReadOnlyProperty<Any, B> {

    private var value: B? = null

    override fun getValue(thisRef: Any, property: KProperty<*>): B {
        val existed = value
        return if (existed != null) {
            existed
        } else {
            val view = viewProvider.invoke()
            val bindMethod = clazz.declaredMethods.firstOrNull {
                val types = it.parameterTypes
                types.size == 1 && types.contains(View::class.java)
            }
            check(bindMethod != null) {
                "No appropriate bind method found. Looks like ViewBinding api has been changed"
            }
            val newInstance = bindMethod.invoke(null, view) as? B
            check(newInstance != null) {
                "Something wrong with ViewBinding plugin. Can't instantiate a binder via reflection"
            }
            value = newInstance
            newInstance
        }
    }

}

class LayoutInflaterBindingLazy<B : ViewBinding>(
    private val clazz: Class<B>,
    private val layoutInflaterProvider: () -> LayoutInflater
) : ReadOnlyProperty<Any, B> {

    private var value: B? = null

    override fun getValue(thisRef: Any, property: KProperty<*>): B {
        val existed = value
        return if (existed != null) {
            existed
        } else {
            val layoutInflater = layoutInflaterProvider.invoke()
            val bindMethod = clazz.declaredMethods.firstOrNull {
                val types = it.parameterTypes
                types.size == 1 && types.contains(LayoutInflater::class.java)
            }
            check(bindMethod != null) {
                "No appropriate inflate method found. Looks like ViewBinding api has been changed"
            }
            val newInstance = bindMethod.invoke(null, layoutInflater) as? B
            check(newInstance != null) {
                "Something wrong with ViewBinding plugin. Can't instantiate a binder via reflection"
            }
            value = newInstance
            newInstance
        }
    }

}