package ru.frosteye.ovsa.commons.marker

interface Hideable {

    var isHidden: Boolean

}