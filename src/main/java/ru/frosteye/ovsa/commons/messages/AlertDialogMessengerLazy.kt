package ru.frosteye.ovsa.commons.messages

import android.content.Context
import androidx.fragment.app.Fragment

class AlertDialogMessengerLazy(
    private val context: Context
): Lazy<Messenger> {

    private var cached: Messenger? = null

    override val value: Messenger
        get() {
            var value = cached
            if (value == null) {
                value = AlertDialogMessenger(context)
                cached = value
            }
            return value
        }

    override fun isInitialized(): Boolean {
        return cached != null
    }
}

class AlertDialogMessengerFragmentLazy(
    private val fragment: Fragment
): Lazy<Messenger> {

    private var cached: Messenger? = null

    override val value: Messenger
        get() {
            var value = cached
            if (value == null) {
                value = AlertDialogMessenger(fragment.requireContext())
                cached = value
            }
            return value
        }

    override fun isInitialized(): Boolean {
        return cached != null
    }
}