package ru.frosteye.ovsa.commons.messages


class AppQuestion(
    val question: CharSequence?,
    val title: CharSequence? = null,
    val positiveVariant: CharSequence? = null,
    val negativeVariant: CharSequence? = null,
    val neutralVariant: CharSequence? = null
) {

    enum class Result {
        POSITIVE,
        NEGATIVE,
        NEUTRAL
    }
}