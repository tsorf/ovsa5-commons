package ru.frosteye.ovsa.commons.messages

import android.app.Activity
import androidx.fragment.app.Fragment

fun Activity.defaultMessenger(): AlertDialogMessengerLazy {
    return AlertDialogMessengerLazy(this)
}

fun Fragment.defaultMessenger(): AlertDialogMessengerFragmentLazy {
    return AlertDialogMessengerFragmentLazy(this)
}