package ru.frosteye.ovsa.commons.messages

import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single


interface Messenger {

    fun question(question: AppQuestion): Maybe<AppQuestion.Result>

    fun message(message: AppMessage): Single<Boolean>
}