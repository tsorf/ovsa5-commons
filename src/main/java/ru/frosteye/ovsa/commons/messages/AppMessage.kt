package ru.frosteye.ovsa.commons.messages

sealed class AppMessage(
    val message: CharSequence?,
    val title: CharSequence?,
    val action: CharSequence?
) {

    class Info internal constructor(
        message: CharSequence?,
        title: CharSequence?,
        action: CharSequence?,
        val code: Int
    ) : AppMessage(message, title, action)

    class Failure internal constructor(
        message: CharSequence?,
        title: CharSequence?,
        action: CharSequence?,
        val code: Int,
        val error: Throwable?
    ) : AppMessage(message, title, action)

    class Success internal constructor(
        message: CharSequence?,
        title: CharSequence?,
        action: CharSequence?,
        val code: Int,
        val payload: Any?
    ) : AppMessage(message, title, action)

    companion object {

        fun info(
            message: CharSequence?,
            title: CharSequence? = null,
            action: CharSequence? = null,
            code: Int = 0
        ): AppMessage {
            return Info(message, title, action, code)
        }

        fun error(
            message: CharSequence?,
            title: CharSequence? = null,
            action: CharSequence? = null,
            code: Int = 0,
            error: Throwable?
        ): AppMessage {
            return Failure(message, title, action, code, error)
        }

        fun success(
            message: CharSequence?,
            title: CharSequence? = null,
            action: CharSequence? = null,
            code: Int = 0,
            payload: Any?
        ): AppMessage {
            return Success(message, title, action, code, payload)
        }
    }
}