package ru.frosteye.ovsa.commons.messages

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.subjects.MaybeSubject
import io.reactivex.rxjava3.subjects.SingleSubject
import ru.frosteye.ovsa.commons.R
import javax.inject.Inject

class AlertDialogMessenger @Inject constructor(
    private val context: Context
) : Messenger {

    override fun question(question: AppQuestion): Maybe<AppQuestion.Result> {

        val result = MaybeSubject.create<AppQuestion.Result>()
        val builder = AlertDialog.Builder(context).apply {
            setTitle(question.title)
            setMessage(question.question)
            setPositiveButton(
                question.positiveVariant ?: context.getString(R.string.ovsa_default_ok)
            ) { dialog: DialogInterface, _: Int ->
                dialog.dismiss()
                result.onSuccess(AppQuestion.Result.POSITIVE)
            }
            setNegativeButton(
                question.negativeVariant ?: context.getString(R.string.ovsa_default_cancel)
            ) { dialog: DialogInterface, _: Int ->
                dialog.dismiss()
                result.onSuccess(AppQuestion.Result.NEGATIVE)
            }
            if (question.neutralVariant != null) {
                setNegativeButton(
                    question.neutralVariant
                ) { dialog: DialogInterface, _: Int ->
                    dialog.dismiss()
                    result.onSuccess(AppQuestion.Result.NEUTRAL)
                }
            }
            setOnCancelListener {
                result.onComplete()
            }
        }

        try {
            builder.show()
        } catch (e: Exception) {
            e.printStackTrace()
            result.onError(e)
        }
        return result
    }

    override fun message(message: AppMessage): Single<Boolean> {
        val result = SingleSubject.create<Boolean>()
        val builder = AlertDialog.Builder(context).apply {
            setTitle(
                message.title ?: when (message) {
                    is AppMessage.Info -> context.getString(R.string.ovsa_default_message_title)
                    is AppMessage.Success -> context.getString(R.string.ovsa_default_message_success_title)
                    is AppMessage.Failure -> context.getString(R.string.ovsa_default_message_error_title)
                }
            )
            setMessage(message.message)
            setPositiveButton(
                message.action ?: context.getString(R.string.ovsa_default_ok)
            ) { dialog: DialogInterface, _: Int ->
                dialog.dismiss()
                result.onSuccess(true)
            }
            setOnCancelListener {
                result.onSuccess(false)
            }
        }

        try {
            builder.show()
        } catch (e: Exception) {
            e.printStackTrace()
            result.onError(e)
        }
        return result
    }
}