package ru.frosteye.ovsa.commons.exceptions

import java.lang.IllegalStateException

class WrongWayException : IllegalStateException("You shouldn't be here!")

fun wrongWay(): Nothing {
    throw WrongWayException()
}