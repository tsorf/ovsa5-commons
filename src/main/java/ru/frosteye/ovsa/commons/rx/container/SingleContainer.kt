package ru.frosteye.ovsa.commons.rx.container

import io.reactivex.rxjava3.core.Single

interface SingleContainer<T : Any> {

    fun getSingle(): Single<T>
}