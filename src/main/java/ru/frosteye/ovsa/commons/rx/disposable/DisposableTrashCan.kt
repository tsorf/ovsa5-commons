package ru.frosteye.ovsa.commons.rx.disposable

import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable


interface DisposableTrashCan {

    val disposableContainer: CompositeDisposable

    operator fun Disposable.unaryPlus() {
        disposableContainer.add(this)
    }

    operator fun Disposable.unaryMinus() {
        disposableContainer.remove(this)
    }

    fun disposeTrashCan() {
        disposableContainer.dispose()
    }
}

class SimpleDisposableTrashCan : DisposableTrashCan {

    override val disposableContainer = CompositeDisposable()

}