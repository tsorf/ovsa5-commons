package ru.frosteye.ovsa.commons.rx.container

import io.reactivex.rxjava3.core.Observable

interface ObservableContainer<T : Any> {

    fun getObservable(): Observable<T>
}