package ru.frosteye.ovsa.commons.rx.container

import io.reactivex.rxjava3.core.Maybe

interface MaybeContainer<T> {

    fun getMaybe(): Maybe<T>
}