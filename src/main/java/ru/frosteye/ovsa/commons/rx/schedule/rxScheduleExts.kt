package ru.frosteye.ovsa.commons.rx.schedule

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers


fun <T : Any> Observable<T>.async(): Observable<T> {
    return this.subscribeOnIo().observeOnUi()
}

fun <T> Maybe<T>.async(): Maybe<T> {
    return this.subscribeOnIo().observeOnUi()
}

fun Completable.async(): Completable {
    return this.subscribeOnIo().observeOnUi()
}

fun <T : Any> Single<T>.async(): Single<T> {
    return this.subscribeOnIo().observeOnUi()
}

fun <T : Any> Observable<T>.subscribeOnIo(): Observable<T> {
    return this.subscribeOn(Schedulers.io())
}

fun <T> Maybe<T>.subscribeOnIo(): Maybe<T> {
    return this.subscribeOn(Schedulers.io())
}

fun Completable.subscribeOnIo(): Completable {
    return this.subscribeOn(Schedulers.io())
}

fun <T : Any> Single<T>.subscribeOnIo(): Single<T> {
    return this.subscribeOn(Schedulers.io())
}

fun <T : Any> Observable<T>.observeOnUi(): Observable<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}

fun <T> Maybe<T>.observeOnUi(): Maybe<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}

fun Completable.observeOnUi(): Completable {
    return this.observeOn(AndroidSchedulers.mainThread())
}

fun <T : Any> Single<T>.observeOnUi(): Single<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}